#ifndef MATRIX_ADDITIONAL_HPP
#define MATRIX_ADDITIONAL_HPP

#include "matrix.hpp"
#include "../gen/mygen.hpp"

/*
 * Замена процедуре "abs" из math.h
 */
double mod( double );

/*
 * Специальное суммирование в матрице
 */
elem_t matrix_special_sum( MATRIX *, size_t, size_t );


/*
 * Специальное суммирование в матрице #2
 */
elem_t matrix_special_sum_2( MATRIX *, elem_t *, size_t, size_t );


/*
 * Перестановка двух строк в матрице
 */
void matrix_move_row( MATRIX *, size_t, size_t );


/*
 * Перестановка двух столбцов в матрице
 */
void matrix_move_column( MATRIX *, size_t, size_t );


/*
 * Нахождение номера максимальное строки
 */
int matrix_find_max( MATRIX *, size_t );


/*
 * Норма матрицы
 */
elem_t norm( MATRIX * );


/*
 * Невязка
 */
double residual( MATRIX *, MATRIX * );


/*
 * Абсолютная ошибка
 */
double err_absolute( MATRIX *, MATRIX * );


/*
 * Относительная ошибка
 */
double err_relative( MATRIX *, MATRIX * );


/*
 * Число обусловленности
 */
double condition_number( MATRIX *, MATRIX * );


/*
 * Генерация матрицы из генератора
 */
void generate(MATRIX *, MATRIX *, int, double, double, int, int, int, int);

#endif /* MATRIX_ADDITIONAL_HPP */

