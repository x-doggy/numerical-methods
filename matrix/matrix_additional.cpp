#include "matrix_additional.hpp"

double mod(double d) {
  return ( d < 0 ) ? -d : d;
}

elem_t matrix_special_sum(MATRIX *m, size_t ii, size_t jj) {

  elem_t s = 0.;

  if (ii > jj) {

    for (int j = 0; j < jj; j++) {
      s += m->matr[ii][j] * m->matr[j][jj];
    }

  } else {

    for (int i = 0; i < ii; i++) {
      s += m->matr[i][jj] * m->matr[ii][i];
    }

  }

  return s;
}

elem_t matrix_special_sum_2(MATRIX *m, elem_t *mas, size_t ii, size_t jj) {

  elem_t s = 0.;
  size_t size = m->row;

  if (ii > jj) {

    for (int k = jj + 1; k < size; k++) {
      s += mas[k] * m->matr[ii][k];
    }

  } else if (ii == jj) {

    for (int k = ii + 1; k < size; k++) {
      s += mas[k] * m->matr[k][ii];
    }

  } else if (ii < jj) {

    for (int k = ii + 1; k < size; k++) {
      s += mas[k] * m->matr[k][jj];
    }

  }

  return s;
}

void matrix_move_row(MATRIX *m, size_t ii, size_t jj) {

  for (int j = 0; j < m->row; j++) {

    elem_t vp = m->matr[ii][j];
    m->matr[ii][j] = m->matr[jj][j];
    m->matr[jj][j] = vp;

  }
}

void matrix_move_column(MATRIX *m, size_t ii, size_t jj) {

  for (int i = 0; i < m->col; i++) {

    elem_t vp = m->matr[i][ii];
    m->matr[i][ii] = m->matr[i][jj];
    m->matr[i][jj] = vp;

  }
}

int matrix_find_max(MATRIX *m, size_t k) {

  size_t size = m->row;
  if (k == size - 1)
    return k;

  int row = k;

  elem_t max = m->matr[k][k] - matrix_special_sum(m, k, k);
  elem_t vp = 0;

  for (int i = k + 1; i < size; i++) {

    vp = m->matr[i][k] - matrix_special_sum(m, i, k);

    if (mod(max) < mod(vp)) {
      max = vp;
      row = i;
    }
  }

  return row;
}

elem_t norm(MATRIX *m) {

  elem_t norm = mod(m->matr[0][0]);
  size_t SIZE = m->row;

  for (int i = 0; i < SIZE; i++) {
    for (int j = 0; j < SIZE; j++) {

      elem_t newnorm = mod(m->matr[i][j]);
      if (newnorm > norm) {
        norm = newnorm;
      }

    }
  }

  return norm * SIZE;
}

double residual(MATRIX *m1, MATRIX *m2) {

  size_t SIZE = m1->row;
  MATRIX *tmp = matrix_mult(m1, m2);

  for (int i = 0; i < SIZE; i++) {
    --(tmp->matr[i][i]);
  }

  return norm(tmp) / SIZE;
}

double err_absolute(MATRIX *m1, MATRIX *m2) {

  size_t SIZE = m1->row;
  MATRIX *tmp = (MATRIX *) matrix_create(m1->row, m2->col);

  for (int i = 0; i < SIZE; i++) {
    for (int j = 0; j < SIZE; j++) {
      tmp->matr[i][j] = m2->matr[i][j] - m1->matr[i][j];
    }
  }

  return norm(tmp);
}

double err_relative(MATRIX *m1, MATRIX *m2) {
  return err_absolute(m2, m1) / norm(m1);
}

double condition_number(MATRIX *m1, MATRIX *m2) {
  return norm(m1) * norm(m2);
}

void generate(MATRIX *m, MATRIX *m_inv, int n, double alpha, double beta, int sign_law, int lambda_law, int variant, int schema) {
  
  mygen(m->matr, m_inv->matr, n, alpha, beta, sign_law, lambda_law, variant, schema);
  m->row = n;
  m->col = n;
  m_inv->row = n;
  m_inv->col = n;
}