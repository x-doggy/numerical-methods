#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <cstdio>
#include <cstdlib>
#include <cstddef>
#include <cassert>

/*
 * Matrix elem definition.
 */
typedef double elem_t;


/*
 * Common structure of matrix.
 */
typedef struct {
  elem_t **matr;
  size_t row;
  size_t col;
} MATRIX;


/*
 * Empty matrix creation.
 */
void *matrix_create( size_t row, size_t col );


/*
 * Freeing matrix.
 */
void matrix_free( MATRIX * );


/*
 * Copying matrix.
 */
void *matrix_copy( MATRIX * );

/*
 * Matrix printing.
 */
void matrix_print( MATRIX * );


/*
 * Matrix creation from stream.
 */
void *matrix_import( FILE *stream );


/*
 * Matrix addition.
 */
MATRIX *matrix_add( MATRIX *, MATRIX * );


/*
 * Matrix substraction.
 */
MATRIX *matrix_substract( MATRIX *, MATRIX * );


/*
 * Matrix multiplication
 */
MATRIX *matrix_mult( MATRIX *, MATRIX * );


#endif /* MATRIX_HPP */

