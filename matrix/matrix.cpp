#include "matrix.hpp"


/*
 * Empty matrix creation.
 */
void *matrix_create( size_t row, size_t col ) {

  elem_t **matrix = (elem_t **) calloc( row, sizeof(elem_t *) );

  for (int i = 0; i < row; i++) {
    matrix[i] = (elem_t *) calloc( row, sizeof(elem_t) );
  }

  MATRIX *m = (MATRIX *) malloc( sizeof(MATRIX) );
  m->matr = matrix;
  m->row = row;
  m->col = col;

  return (void *) m;
}


/*
 * Freeing matrix.
 */
void matrix_free( MATRIX *m ) {

  for (int i = 0; i < m->row; i++) {
    free(m->matr[i]);
  }

  free(m->matr);
  free(m);
}


/*
 * Copying matrix.
 */
void *matrix_copy( MATRIX *m ) {

  MATRIX *copy = (MATRIX *) matrix_create(m->row, m->col);
  
  for (int i = 0; i < m->row; i++) {
    for (int j = 0; j < m->col; j++) {
      
      copy->matr[i][j] = m->matr[i][j];
      
    }
  }
  
  return (void *) copy;
}


/*
 * Printing matrix.
 */
void matrix_print( MATRIX *m ) {

  for (int i = 0; i < m->row; i++) {

    for (int j = 0; j < m->col; j++) {
      printf( "%14.4e", (m->matr)[i][j] );
    }

    /* Line feed */
    puts("");

  }

  puts("\n");
}


/*
 * Matrix creation from stream.
 */
void *matrix_import( FILE *stream ) {

  int rows, columns;

  fread(&rows   , sizeof(int), 1, stream);
  fread(&columns, sizeof(int), 1, stream);

  /* Dynamically create 2D array and add data to it from input stream */
  MATRIX *m = (MATRIX *) matrix_create( rows, columns );

  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < columns; j++) {

      fread(&m->matr[i][j], sizeof(double), 1, stream);

    }
  }

  return (void *) m;
}


/*
 * Matrix addition.
 */
MATRIX *matrix_add( MATRIX *m1, MATRIX *m2 ) {

  assert(m1 && m2 && m1->row == m2->row && m1->col == m2->col);

  MATRIX *result = (MATRIX *) matrix_create( m1->row, m2->col );

  for (int i = 0; i < m1->row; i++) {
    for (int j = 0; j < m2->col; j++) {

      result->matr[i][j] = m1->matr[i][j] + m2->matr[i][j];

    }
  }

  return result;
}


/*
 * Matrix substraction.
 */
MATRIX *matrix_substract( MATRIX *m1, MATRIX *m2 ) {

  assert(m1 && m2 && m1->row != m2->row && m1->col != m2->col);

  MATRIX *result = (MATRIX *) matrix_create( m1->row, m2->col );

  for (int i = 0; i < m1->row; i++) {
    for (int j = 0; j < m2->col; j++) {

      result->matr[i][j] = m1->matr[i][j] - m2->matr[i][j];

    }
  }

  return result;
}


/*
 * Matrix multiplication
 */
MATRIX *matrix_mult( MATRIX *m1, MATRIX *m2 ) {

  assert(m1 && m2 && m1->col == m2->row);

  MATRIX *result = (MATRIX *) matrix_create( m1->row, m2->col );

  for (int i = 0; i < m1->row; i++) {
    for (int j = 0; j < m2->col; j++) {
      for (int k = 0; k < m2->row; k++) {

        result->matr[i][j] += m1->matr[i][k] * m2->matr[k][j];

      }
    }
  }

  return result;
}
