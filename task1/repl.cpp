#include <cstdio>
#include <cstdlib>

void func(char c) {
  printf("%c = %d\n", c, (int) c);
}

int main(int argc, char **argv) {
  int c;

  do {
    printf("%s", "matrix_app> ");
    c = getchar();
    fflush(stdin);
    if (c == 10) continue;
    if (c == EOF) break;

    func(c);
  } while (c != EOF);

  return 0;
}
