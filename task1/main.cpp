#include <cstdio>
#include <cstdlib>
#include <ctime>
#include "lu.hpp"

int main(int argc, char **argv) {

  clock_t start, finish;
  clock_t startmethod, finishmethod;

  MATRIX *m1, *m2, *m1_inv;

  m1     = (MATRIX *) matrix_create(N, N);
  m1_inv = (MATRIX *) matrix_create(N, N);

  double gamma = 0.;    /* Невязка */
  double err_abs = 0.;  /* Абсолютная ошибка */
  double err_rel = 0.;  /* Относительная ошибка */
  double nyu = 0.;      /* Обусловленность */

  start = clock();

  generate(m1, m1_inv, N, ALPHA, BETA, 0, 0, 2, 1);
  m2 = (MATRIX *) matrix_copy(m1);

//  puts("Generated matrix m1:\n");
//  matrix_print(m1);
//  puts("generated matrix m1_inv:\n");
//  matrix_print(m1_inv);

  startmethod = clock();
  LU(m1);
  finishmethod = clock();
  
//  puts("Matrix after LU-decomposing:\n");
//  matrix_print(m1);

  gamma = residual(m2, m1);

  err_abs = err_absolute(m2, m1);
  err_rel = err_relative(m2, m1);

  nyu = condition_number(m1, m2);

  finish = clock();

  double elapsedmethodtime = (double) (finishmethod - startmethod) / CLOCKS_PER_SEC;
  printf("Время выполнения метода = %f сек.\n", elapsedmethodtime);

  double elapsedtime = (double) (finish - start) / CLOCKS_PER_SEC;
  printf("Время выполнения всех вычислений = %f сек.\n\n", elapsedtime);

  printf("Невязка = %1.2e\n", gamma);
  printf("Абсолютная ошибка = %1.2e\n", err_abs);
  printf("Относительная ошибка = %1.2e\n", err_rel);
  printf("Обусловленность = %1.2e\n", nyu);

  matrix_free(m2);
  matrix_free(m1_inv);
  matrix_free(m1);

  return 0;
}

