cmake_minimum_required(VERSION 3.6)
project(numerical-methods)
set(CMAKE_CXX_STANDARD 11)

set(SOURCE_DIR task1)

add_library(lu STATIC lu.cpp)
add_executable(${SOURCE_DIR} main.cpp)
target_link_libraries(${SOURCE_DIR} lu matrix_additional matrix mygen)
