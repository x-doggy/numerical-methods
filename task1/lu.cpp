#include "lu.hpp"

/*
 * LU-разложение с выбором главного элемента
 */
void LU( MATRIX *matr ) {

  size_t const size = matr->row;
  int i, j, k;
  int vp = 0;

  /* Массив перестановок */
  int trans[size * 2];

  /* Последняя перестановка */
  int last = 0;

  /* Вспомогательный массив */
  double mas[size * 2];

  /*
   * +-------------------------------------------+
   * | LU-разложение с выбором главного элемента |
   * +-------------------------------------------+
   */
  double maxEl = matr->matr[0][0];

  for (i = 1; i < size; i++) {
    if (mod(maxEl) < mod(matr->matr[i][0])) {
      vp = i;
      maxEl = matr->matr[vp][0];
    }
  }

  if (vp != 0) {
    matrix_move_row(matr, 0, vp);
    trans[0] = 0;
    trans[1] = vp;
    last = 1;
  }

  assert(matr->matr[0][0] != eps);

  for (i = 1; i < size; i++) {
    matr->matr[i][0] /= matr->matr[0][0];
  }

  /* Цикл по диагонали */
  for (k = 1; k < size; k++) {
    vp = matrix_find_max(matr, k);
    if (vp != k) {
      matrix_move_row(matr, k, vp);
      trans[last + 1] = k;
      trans[last + 2] = vp;
      last += 2;
    }

    for (j = k; j < size; j++) {
      matr->matr[k][j] -= matrix_special_sum(matr, k, j);
    }
    
    for (i = k + 1; i < size; i++) {
      assert(matr->matr[k][k] != eps);
      matr->matr[i][k] = (matr->matr[i][k] - matrix_special_sum(matr, i, k)) / matr->matr[k][k];
    }

  }



  /*
   * +-------------------+
   * | Обращение матрицы |
   * +-------------------+
   */
  
  assert(matr->matr[size - 1][size - 1] != eps);

  matr->matr[size - 1][size - 1] = 1 / matr->matr[size - 1][size - 1];

  /* Цикл по диагонали */
  for (k = size - 2; k >= 0; k--) {

    /* Для L */
    for (i = k + 1; i < size; i++) {
      mas[i] = matr->matr[i][k];
    }
    
    for (i = k + 1; i < size; i++) {
      matr->matr[i][k] = - matrix_special_sum_2(matr, mas, i, k);
    }

    /* Для U */
    for (j = k; j < size; j++) {
      mas[j] = matr->matr[k][j];
    }

    matr->matr[k][k] = (1 - matrix_special_sum_2(matr, mas, k, k)) / mas[k];

    for (j = k + 1; j < size; j++) {
      matr->matr[k][j] = (-1) * matrix_special_sum_2(matr, mas, k, j) / mas[k];
    }
  }

  for (i = last; last > 0; last--) {
    matrix_move_row(matr, trans[last], trans[last - 1]);
  }

}