#ifndef LU_HPP
#define LU_HPP
  
#include "../matrix/matrix.hpp"
#include "../matrix/matrix_additional.hpp"
#include "../gen/mygen.hpp"

#define eps 0.

void LU( MATRIX * );

#endif /* LU_HPP */

