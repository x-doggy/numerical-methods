#include <iostream>
#include <cmath>
#include <cassert>
#include "../gen/mygen.hpp"

using namespace std;

// вычисление нормы
double norm(double *z, int n) {

  double max = fabs(z[0]);
  for (int i = 1; i < n; i++) {
    if (fabs(z[i]) > max)
      max = fabs(z[i]);
  }

  return max;
}

// вычисление невязки
void rcalcul(int n, double **a, double *f, double *x, double *r) {

  for (int i = 0; i < n; i++) {
    r[i] = -f[i];
    for (int j = 0; j < n; j++) {
      r[i] += a[i][j] * x[j];
    }
  }
}

// метод Якоби
int jacobi(int n, double **a, double *f, double *x, double eps) {

  double *xp = new double[n];
  double norm, ksi, d_inv;
  int it = 0;

  // предобработка.
  for (int i = 0; i < n; i++) {
    assert (a[i][i] != 0.);
    d_inv = 1. / a[i][i];
    for (int j = 0; j < i; j++) a[i][j] *= d_inv;
    for (int j = i + 1; j < n; j++) a[i][j] *= d_inv;
    f[i] *= d_inv;
  }

  do {
    it++;
    for (int i = 0; i < n; i++) {
      xp[i] = f[i];
      for (int j = 0; j < n; j++) {
        if (i != j)
          xp[i] -= a[i][j] * x[j];
      }
      assert(a[i][i] != 0.);
      xp[i] /= a[i][i];
    }
    norm = fabs(x[0] - xp[0]);
    for (int i = 0; i < n; i++) {
      if ((ksi = fabs(x[i] - xp[i])) > norm)
        norm = ksi;
      x[i] = xp[i];
    }
    
  } while (norm > eps && it <= 1e5);

  delete[] xp;

  return it;
}

int main() {

  int const n = 400;
  double const alpha = 1.e-4;
  double const beta = 1.;
  double eps = 1.e-1;

  while (eps > 1.e-10) {

    cout << "-------------------" << endl;
    cout << "EPS = " << eps << endl;

    //создание матрицы
    double ** matrix = new double*[n];
    for (int i = 0; i < n; i++) {
      matrix[i] = new double[n];
    }

    // генерация матрицы
    // mygen(matrix, matrix2, n, alpha, beta, 1, 2, 0, 1);

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        if (i == j) matrix[i][j] = 2.;
        else if (fabs(i - j) == 1) matrix[i][j] = -1.;
        else matrix[i][j] = 0.;
      }
    }

    // копия матрицы
    double ** matrix2 = new double*[n];
    for (int i = 0; i < n; i++) {
      matrix2[i] = new double[n];
    }
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++)
        matrix2[i][j] = matrix[i][j];
    }

    // xt - вектор точного решения
    double *xt = new double[n];
    for (int i = 0; i < n; i++) {
      xt[i] = sin(i) * sqrt(i);
    }

    // хp - вектор приближенного значения
    double *xp = new double[n];
    for (int i = 0; i < n; i++) {
      xp[i] = 0.;
    }

    //правая часть
    double *f = new double[n];
    double *f2 = new double[n];
    for (int i = 0; i < n; i++) {
      f[i] = 0;
      for (int j = 0; j < n; j++) {
        f[i] += matrix[i][j] * xt[j];
      }
      f2[i] = f[i];
    }

    int it = jacobi(n, matrix, f, xp, eps);
    cout << "iterations number = " << it << endl;
    cout << "xp (" << xp[0] << "; " << xp[1] << "; " << xp[2] << ")" << endl;

    // вектор ошибки
    double *z = new double[n];
    for (int i = 0; i < n; i++)
      z[i] = xp[i] - xt[i];
    cout << "z (" << z[0] << "; " << z[1] << "; " << z[2] << ")" << endl;

    // абсолютная ошибка
    double znorm = norm(z, n);
    cout << "znorm = " << znorm << endl;

    // относительная ошибка
    double dz = znorm / norm(xt, n);
    cout << "dz = " << dz << endl;

    // невязка
    double *r = new double[n];
    rcalcul(n, matrix2, f2, xp, r);
    cout << "r (" << r[0] << "; " << r[1] << "; " << r[2] << ")" << endl;

    // норма невязки
    double rnorm = norm(r, n);
    cout << "rnorm = " << rnorm << endl;

    // относительная норма невязки
    double rho = rnorm / norm(f2, n);
    cout << "rho = " << rho << endl;

    eps /= 10;

    for (int i = 0; i < n; i++) {
      delete[] matrix2[i];
      delete[] matrix[i];
    }
    delete[] matrix2;
    delete[] matrix;
    delete[] r;
    delete[] f;
    delete[] f2;
    delete[] xt;
    delete[] xp;
  }
  return 0;
}
