#ifndef MYGEN_HPP
#define MYGEN_HPP

#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdio>
using namespace std;

#define N 100
#define ALPHA 1.e-1
#define BETA 1.


/*//*/
void mygen ( double **a, double **a_inv, int n, double alpha, double beta, int sign_law, int lambda_law, int variant, int schema );
void Q_matrix ( double **Q, int n, int schema );
void matr_mul( double **a,  double **b,  double **c, int n );
double matr_inf_norm ( double **a, int n );
/*//*/



#endif /* MYGEN_HPP */

